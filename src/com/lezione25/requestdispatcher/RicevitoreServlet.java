package com.lezione25.requestdispatcher;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RicevitoreServlet extends HttpServlet{

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		String var_nome = request.getParameter("input_nome");
		String var_cognome = request.getParameter("input_cognome");
		
		if(var_cognome.isEmpty() || var_nome.isEmpty()) {
			PrintWriter out = response.getWriter();
			out.print("ERRORE NEL FORM!");
			return;
		}
		
		/*
		 * Modo per aggiungere parametri all'interno di una request
		 * ma, dato che i parametri provengono solo dal FORM, posso
		 * ovviare a questo problema aggiungendo degli "attributi"
		 */
		
		request.setAttribute("ruolo", 3);
		
		RequestDispatcher rd = request.getRequestDispatcher("elaboratore");
		rd.forward(request, response);
		
	}
	
}
