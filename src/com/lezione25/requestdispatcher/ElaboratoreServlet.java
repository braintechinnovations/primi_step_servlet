package com.lezione25.requestdispatcher;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ElaboratoreServlet extends HttpServlet{

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String var_nome = request.getParameter("input_nome");
		String var_cognome = request.getParameter("input_cognome");
		
		Integer att_ruolo = (Integer) request.getAttribute("ruolo");
		
		PrintWriter out = response.getWriter();
		out.print("Benvenuto " + att_ruolo + " - " + var_nome + " " + var_cognome);
	}

	
}
