package com.lezione25.servlettest;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SommatoreServlet extends HttpServlet{
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		int var_uno = Integer.parseInt(request.getParameter("numero_uno"));
		int var_due = Integer.parseInt(request.getParameter("numero_due"));
		int var_tre = Integer.parseInt(request.getParameter("numero_tre"));
		
//		System.out.println("Sono la servlet Sommatore");
//		System.out.println("Numero uno: " + var_uno);
//		System.out.println("Numero due: " + var_due);
		
		int somma = var_uno + var_due + var_tre;
		
		PrintWriter scrittore = response.getWriter();
		scrittore.print(somma);
	}
	
}
