package com.lezione25.servlettest;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletMetodi extends HttpServlet{

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String var_nome = request.getParameter("input_nome");
		String var_cognome = request.getParameter("input_cognome");
	
		
		PrintWriter out = response.getWriter();
		out.println("POST - " + var_nome + ", " + var_cognome);

	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		out.println("Attenzione, il GET non � permesso!");
	}

	
}
