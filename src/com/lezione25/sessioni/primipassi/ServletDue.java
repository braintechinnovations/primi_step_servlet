package com.lezione25.sessioni.primipassi;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletDue extends HttpServlet{
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
	
		HttpSession sessioncina = request.getSession();
		String var_nome = (String) sessioncina.getAttribute("nome");
		String var_cognome = (String) sessioncina.getAttribute("cognome");
		Integer var_eta = (Integer) sessioncina.getAttribute("eta");
		
		PrintWriter out = response.getWriter();
		out.print("Servlet DUE " + var_nome + " " + var_cognome + " " + var_eta);
		
	}
	
}
