package com.lezione25.sessioni.primipassi;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletUno extends HttpServlet{

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession sessione = request.getSession();
		sessione.setAttribute("nome", "Giovanni");
		sessione.setAttribute("cognome", "Pace");
		sessione.setAttribute("eta", 34);
		
	}
	
}
