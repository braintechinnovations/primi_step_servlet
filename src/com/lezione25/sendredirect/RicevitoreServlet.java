package com.lezione25.sendredirect;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RicevitoreServlet extends HttpServlet{

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		String var_nome = request.getParameter("input_nome");
		String var_cognome = request.getParameter("input_cognome");
		
		if(var_cognome.isEmpty() || var_nome.isEmpty()) {
			response.sendRedirect("errore.html");
		}
		
		response.sendRedirect("elaboratoreredirect?input_nome=" + var_nome + "&input_cognome=" + var_cognome);
	}
	
}
